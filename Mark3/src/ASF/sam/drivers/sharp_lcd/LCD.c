/* Copyright (c) 2014 Ambit Network Systems. All Rights Reserved.
 *
 *	The information contained herein is property of Ambit Systems.
 *	Derived from Sharp app note, by Ken Green.
 *
 *  This heading must NOT be removed from the file.
 *	By Mike Wirth, March, 2014.  Loosely based on the LCD
 *	App Note from Sharp, by Ken Green.
 *
 */

/**
 * Driver for a Sharp LCD, LS013B7DH03.
 *	Note: Uses TIMER2 to generate 60 Hz Vcom signal.
 *		
 */

//#ifdef	 AMBIT

#include <board.h>
#include <conf_board.h>

#include "LCD.h"
#include "sleepmgr.h"
#include "flexcom.h"
#include "spi.h"
#include "sysclk.h"
#include "ioport.h"
#include "delay.h"

/* Chip select. */
#define SPI_CHIP_SEL 0
#define SPI_CHIP_PCS spi_get_pcs(SPI_CHIP_SEL)

/* SPI clock setting (Hz). */
static uint32_t gs_ul_spi_clock = 500000;

/* Clock polarity. */
#define SPI_CLK_POLARITY 0

/* Clock phase. */
#define SPI_CLK_PHASE 0

/* Delay before SPCK. */
#define SPI_DLYBS 0x40

/* Delay between consecutive transfers. */
#define SPI_DLYBCT 0x10
/** SPI base address for SPI master mode*/


// Working variables -- track state of frame buffer transfer to LCD.
uint8_t vcom = 0;		// Current state of vcom (pulsed at 60Hz)
static uint8_t *Frmbufptr;	// Pointer to current byte in frame buffer
static uint8_t LcdLineBufr[MLCD_BYTES_PER_LINE + 4];	// Local buffer to assemble write record
static uint8_t LcdLineAddress;	// Current line number being transmitted (limited to 8 bits!)
// NOTE: Counts from 1 to MLCD_YRES; NOT from 0.
//uint8_t  LCDframeBuffer[2];
uint8_t reverseB(uint8_t b){
	uint8_t i, c=0;
	for (i=0;i<=6;i++){
		c |= b & 0x01;
		c = c<<1;
		b = b>>1;	
	}
	c |= b & 0x01;
	return c;
}

#ifdef MANUAL_LCD_CS
static void setLcdChipSelect(void){
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_HIGH);
}

static void clearLcdChipSelect(void){
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_LOW);
}
#endif

/**
 * \brief Initialize SPI as master.
 */
static void spi_master_initialize(void)
{
	//puts("-I- Initialize SPI as master\r");

#if (SAMG55)
	/* Enable the peripheral and set SPI mode. */
	flexcom_enable(BOARD_FLEXCOM_SPI);
	flexcom_set_opmode(BOARD_FLEXCOM_SPI, FLEXCOM_SPI);
#else
	/* Configure an SPI peripheral. */
	spi_enable_clock(SPI_MASTER_BASE);
#endif
	spi_disable(SPI_MASTER_BASE);
	spi_reset(SPI_MASTER_BASE);
	spi_set_lastxfer(SPI_MASTER_BASE);
	spi_set_master_mode(SPI_MASTER_BASE);
	spi_disable_mode_fault_detect(SPI_MASTER_BASE);
	spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_CHIP_PCS);
	spi_set_clock_polarity(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_POLARITY);
	spi_set_clock_phase(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_PHASE);
	spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_CHIP_SEL,
			SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(SPI_MASTER_BASE, SPI_CHIP_SEL,
			(sysclk_get_cpu_hz() / gs_ul_spi_clock));
	spi_set_transfer_delay(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_DLYBS,
			SPI_DLYBCT);
	spi_enable(SPI_MASTER_BASE);
}

/**
 * \brief Perform SPI master transfer.
 *
 * \param pbuf Pointer to buffer to transfer.
 * \param size Size of the buffer.
 */
static void spi_master_transfer(void *p_buf, uint32_t size)
{
	uint32_t i;
	uint8_t uc_pcs;
	static uint16_t data;

	uint8_t *p_buffer;

	p_buffer = p_buf;

#ifdef MANUAL_LCD_CS
	setLcdChipSelect();
#endif 

	for (i = 0; i < size; i++) {
		spi_write(SPI_MASTER_BASE, p_buffer[i], 0, 0);
		/* Wait transfer done. */
		while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);
		spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
		p_buffer[i] = data;
	}

#ifdef MANUAL_LCD_CS
	clearLcdChipSelect();
#endif

}


// Function to clear the Sharp LCD display.
void LCD_clear(void)
{
	//bool result;
	LcdLineBufr[0] = MLCD_CM;						// "Clear Memory" command
	LcdLineBufr[1] = 0x00;							// Trailer byte
	spi_master_transfer(LcdLineBufr,2);	// Transfer 2-byte record (presume success)
	//APP_ERROR_CHECK_BOOL(result);
	LcdLineAddress = 1;											// Set the LCD line number (Y-address) to 1 (ranges from 1 to MLCD_YRES).
}
// Function to initialize the Sharp LCD, including the GPIO lines and SPI
// peripheral on the Nordic ARM chip.
// NOTE: Sequence of operations structured to follow Fig. 4, "Power Supply Sequencing" in document
// "Sharp LS013B7DH03 Application Information", p.7
void LCD_init(void)
{
	// Initialize SPI0 (SCLK, SI lines) and SCS GPIO line.

	spi_master_initialize();

	// T0 and T1 per Fig. 4.
	// Set up GPIO control lines (EXTCOMIN, DISP, EXTMODE)
	ioport_set_pin_dir(EXT1_PIN_LCD_ENABLE,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_dir(EXT1_PIN_LCD_EXTMODE,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_dir(EXT1_PIN_SPI_LCD_EXTCOM,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
#ifdef MANUAL_LCD_CS
	ioport_set_pin_dir(EXT3_PIN_GPIO_0,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_HIGH);
#endif //MANUAL_LCD_CS

	ioport_set_pin_level(EXT1_PIN_LCD_ENABLE, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(EXT1_PIN_SPI_LCD_EXTCOM, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(EXT1_PIN_LCD_EXTMODE, IOPORT_PIN_LEVEL_HIGH);
	
	//delay

	delay_ms(1);										// Wait for LCD latches to settle

	// Fig. 4, T2
	LCD_clear();										// With LCD_DISP off, clear the display
	delay_ms(1);										// For safety, wait a bit.

	LCD_clear();										// With LCD_DISP off, clear the display
	delay_ms(1);										// For safety, wait a bit.

	LCD_clear();										// With LCD_DISP off, clear the display
	delay_ms(1);										// For safety, wait a bit.

	LCD_clear();										// With LCD_DISP off, clear the display
	delay_ms(1);										// For safety, wait a bit.

	// Fig. 4, T3
	ioport_set_pin_level(EXT1_PIN_LCD_ENABLE, IOPORT_PIN_LEVEL_HIGH);	// Raise LCD_DISP to turn on display of pixels, will stay on...
	delay_ms(1);										// For safety, wait a bit.
	return;												// LCD now in running mode
}




